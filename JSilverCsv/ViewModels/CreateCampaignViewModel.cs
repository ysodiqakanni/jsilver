﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JSilverCsv.ViewModels
{
    public class CreateCampaignViewModel
    {
        [Required]
        public int OrderId { get; set; }
        public int CampaignID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int ColA { get; set; } = 0;
        [Required]
        public int ColB { get; set; } = 0;
        [Required]
        public int ColC { get; set; } = 0;
        [Required]
        public int ColD { get; set; } = 0;
        [Required]
        public int ColE0To30 { get; set; } = 0;
        [Required]
        public int ColE31To60 { get; set; } = 0;
        [Required]
        public int ColE61To90 { get; set; } = 0;
        [Required]
        public int ColE91Plus { get; set; } = 0;
        [Required]
        public int Qty { get; set; }
        public string InputFilePath { get; set; }


        [Required]
        [Display(Name = "Import csv file")]
        public HttpPostedFileBase CsvInputFile { get; set; }
    }
    public class CampaignIndexViewModel
    {

    }
    public class EditCampaignViewModel
    {
        public int ID { get; set; }
        [Required]
        public int OrderId { get; set; }
        public int Qty { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int ColA { get; set; } = 0;
        [Required]
        public int ColB { get; set; } = 0;
        [Required]
        public int ColC { get; set; } = 0;
        [Required]
        public int ColD { get; set; } = 0;
        [Required]
        public int ColE0To30 { get; set; } = 0;
        [Required]
        public int ColE31To60 { get; set; } = 0;
        [Required]
        public int ColE61To90 { get; set; } = 0;
        [Required]
        public int ColE91Plus { get; set; } = 0;
   
        [Display(Name = "Upload File")]
        public string InputFilePath { get; set; }

        public string OutputFilePath { get; set; }
    }
}