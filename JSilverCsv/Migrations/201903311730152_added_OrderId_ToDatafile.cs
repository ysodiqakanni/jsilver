namespace JSilverCsv.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_OrderId_ToDatafile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataFiles", "OrderId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DataFiles", "OrderId");
        }
    }
}
