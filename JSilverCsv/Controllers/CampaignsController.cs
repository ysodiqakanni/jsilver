﻿using JSilverCsv.Models;
using JSilverCsv.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace JSilverCsv.Controllers
{
    [Authorize]
    public class CampaignsController : Controller
    {
        ApplicationDbContext ctx;
        public CampaignsController()
        {
            ctx = new Models.ApplicationDbContext();
        }
        // GET: Campaigns
        public ActionResult Index()
        {
            var allDataFIles = ctx.DataFiles.ToList();
            return View(allDataFIles);
        }
        public ActionResult Upload(HttpPostedFileBase file)
        {
            // save input file to disk
            if (file == null || file.ContentLength < 1)
                return new HttpStatusCodeResult(500, "File content is empty!");

            try
            {
                Stream stream = file.InputStream;
                using (var reader = new StreamReader(stream))
                {
                    string dataString = reader.ReadToEnd().Trim();
                    var dataArray = dataString.Split('\n');
                    int numberOfRecords = dataArray.Count() - 1;

                    // now upload file
                    string inputPath = "~/InputFiles/";
                    //var extension = Path.GetExtension(file.FileName);
                    //string ordinaryFileName = Path.GetFileName(file.FileName);
                    //string myFileName = ordinaryFileName + "_" + Guid.NewGuid() + extension;

                    var path = Path.Combine(Server.MapPath(inputPath), file.FileName);
                    file.SaveAs(path);

                    return Json(new { success = true, qty = numberOfRecords, path = inputPath + file.FileName });
                }


            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = "An error occured while uploading file!" });
            }
        }
        public ActionResult Create()
        {
            
            Random rnd = new Random();
            //int x1 = rnd.Next(1,25);
            //int x2 = rnd.Next(1, 25);
            //int rem = 100 - (x1 + x2);
            //int x3 = rnd.Next(rem / 3);
            //int x4 = rem - x3;
            int x1 = rnd.Next(1, 10);
            int x2 = rnd.Next(1, 10);
            int x3 = rnd.Next(1, 10);
            int x4 = rnd.Next(1, 10);
            int sum = x1 + x2 + x3 + x4;
            x1 = x1 * 100 / sum;
            x2 = x2 * 100 / sum;
            x3 = x3 * 100 / sum;
            x4 = x4 * 100 / sum;
            CreateCampaignViewModel model = new ViewModels.CreateCampaignViewModel()
            {
                ColE0To30 = x1,
                ColE31To60 = x2,
                ColE61To90 = x3,
                ColE91Plus = x4
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(CreateCampaignViewModel model)
        {
            if (ModelState.IsValid)
            {
                var outputFileBuilder = new StringBuilder();
                using (var reader = new StreamReader(Server.MapPath(model.InputFilePath)))
                {
                    #region verify input file and form data
                    // get the actual csv data as string and then store in array
                    string dataString = reader.ReadToEnd().Trim();
                    var dataArray = dataString.Split('\n');

                    // verify input data
                    int numberOfRecords = model.Qty;
                    // colA <= colB <= colC <= colD <= number of records
                    if (model.ColA > numberOfRecords || model.ColB > numberOfRecords)
                    {
                        ModelState.AddModelError("", "Values of colA to colD can not be greater than the number of records in file");
                        return View(model);
                    }
                    if (model.ColE0To30 + model.ColE31To60 + model.ColE61To90 + model.ColE91Plus > numberOfRecords)
                    {
                        ModelState.AddModelError("", "Column E records can not be more than the number of records in file");
                        return View(model);
                    }
                    #endregion

                    #region Get header and layout type
                    // Take the header => the first row
                    var firstRowHeader = dataArray[0].Trim();   // trim any new line at the end


                    if (firstRowHeader.Trim().EndsWith(","))
                    {
                        firstRowHeader = firstRowHeader.Trim().TrimEnd(',');
                    }
                    int numberOfColumns = firstRowHeader.Split(',').Count();

                    // Randomize the data skipping the first row
                    Random rnd = new Random();
                    var randomizedArray = dataArray.Skip(1).Where(d => !String.IsNullOrWhiteSpace(d.Replace(',', ' '))).OrderBy(x => rnd.Next()).ToArray();


                    int layoutType = 0; bool extraColumnsAlreadyAppended = false;
                    if (!extraColumnsAlreadyAppended)
                    {
                        // append extra columns to header
                        firstRowHeader += ",Open,Click,Bounced,Optout,Filler1";
                    }
                    outputFileBuilder.AppendLine(firstRowHeader);
                    #endregion

                    #region process data
                    int countC = 0; int countD = 0;
                    // loop through the data and append X where neccessary
                    for (int i = 0; i < randomizedArray.Length; i++)
                    {
                        string row = randomizedArray[i];
                        var rowColumns = row.Split(',');
                        string colA = model.ColA > i ? "Y" : "";
                        string colB = model.ColB > i ? "Y" : "";
                        string colC = "";
                        if(colA == "" && colB == "" && countC < model.ColC)
                        {
                            colC = "Y";
                            countC++;
                        }
                        string colD = "";
                        if (colA == "" && colB == "" && colC == "" && countD < model.ColD)
                        {
                            colD = "Y";
                            countD++;
                        }

                        string colE = string.Empty;
                        if (i < model.ColE0To30)
                        {
                            colE = "0 - 30";
                        }
                        else if (i < (model.ColE0To30 + model.ColE31To60))
                        {
                            colE = "31 - 60";
                        }
                        else if (i < (model.ColE0To30 + model.ColE31To60 + model.ColE61To90))
                        {
                            colE = "61 - 90";
                        }
                        else colE = "91+";

                        // append data to row
                        outputFileBuilder.AppendLine(String.Join(",", row.Trim().TrimEnd(',').Split(',').Take(numberOfColumns).ToArray()) + $",{colA},{colB},{colC},{colD},{colE}");
                    }
                    #endregion

                    CreateNeccessaryFolders();


                    string fileName = Path.GetFileNameWithoutExtension(model.InputFilePath);
                    // save output file
                    string outputPath = "~/OutputFiles/" + fileName + "_Match_QTY"+model.Qty + ".csv";
                    // .AppendAllText does not autoreplace so check if exist then delete
                    RemoveFileFromDisk(outputPath);
                    System.IO.File.AppendAllText(Server.MapPath(outputPath), outputFileBuilder.ToString());

                    DataFile dataFile = new DataFile
                    {
                        ColA = model.ColA,
                        ColB = model.ColB,
                        ColC = model.ColC,
                        ColD = model.ColD,
                        ColE0To30 = model.ColE0To30,
                        ColE31To60 = model.ColE31To60,
                        ColE61To90 = model.ColE61To90,
                        ColE91Plus = model.ColE91Plus,
                        CampaignName = model.Name,
                        InputFileUri = model.InputFilePath,
                        OutputFileUri = outputPath,
                        NumberOfRecords = numberOfRecords,
                        OrderId = model.OrderId
                    };
                    ctx.DataFiles.Add(dataFile);
                    ctx.SaveChanges();

                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateOld(CreateCampaignViewModel model)
        {
            if (ModelState.IsValid)
            {
                var outputFileBuilder = new StringBuilder();
                string fileName = Path.GetFileNameWithoutExtension(model.CsvInputFile.FileName);

                Stream stream = model.CsvInputFile.InputStream;
                using (var reader = new StreamReader(stream))
                {
                    #region verify input file and form data
                    // get the actual csv data as string and then store in array
                    string dataString = reader.ReadToEnd().Trim();
                    var dataArray = dataString.Split('\n');

                    // verify input data
                    int numberOfRecords = dataArray.Count() - 1; // the first record is the header
                    // colA <= colB <= colC <= colD <= number of records
                    if (model.ColA > numberOfRecords || model.ColB > numberOfRecords || model.ColC > numberOfRecords || model.ColD > numberOfRecords)
                    {
                        ModelState.AddModelError("", "Values of colA to colD can not be greater than the number of records in file");
                        return View(model);
                    }
                    if (model.ColE0To30 + model.ColE31To60 + model.ColE61To90 + model.ColE91Plus > numberOfRecords)
                    {
                        ModelState.AddModelError("", "Column E records can not be more than the number of records in file");
                        return View(model);
                    }
                    #endregion

                    #region Get header and layout type
                    // Take the header => the first row
                    var firstRowHeader = dataArray[0];
                    if (firstRowHeader.Trim().EndsWith(","))
                    {
                        firstRowHeader = firstRowHeader.Trim().TrimEnd(',');
                    }
                    int numberOfColumns = firstRowHeader.Split(',').Count();

                    // Randomize the data skipping the first row
                    Random rnd = new Random();
                    var randomizedArray = dataArray.Skip(1).Where(d => !String.IsNullOrWhiteSpace(d.Replace(',', ' '))).OrderBy(x => rnd.Next()).ToArray();

                    // check the first row if it contains col A to E
                    // first layout 5 columns,  second layout 18 columns


                    int layoutType = 0; bool extraColumnsAlreadyAppended = false;
                    if (numberOfColumns == 1) layoutType = 1;
                    else if (numberOfColumns > 1 && numberOfColumns <= 6)
                    {
                        layoutType = 1;
                        extraColumnsAlreadyAppended = true;
                    }
                    else if (numberOfColumns == 18)
                    {
                        layoutType = 2;
                    }

                    if (!extraColumnsAlreadyAppended)
                    {
                        // append extra columns to header
                        firstRowHeader += ",Column A, Column B, Column C, Column D, Column E";
                    }
                    outputFileBuilder.AppendLine(firstRowHeader);
                    #endregion

                    #region process data
                    // loop through the data and append X where neccessary
                    for (int i = 0; i < randomizedArray.Length; i++)
                    {
                        string row = randomizedArray[i];
                        var rowColumns = row.Split(',');
                        string colA = model.ColA > i + 1 ? "X" : "";
                        string colB = model.ColB > i + 1 ? "X" : "";
                        string colC = model.ColC > i + 1 ? "X" : "";
                        string colD = model.ColD > i + 1 ? "X" : "";
                        string colE = string.Empty;
                        if (i < model.ColE0To30)
                        {
                            colE = "0 - 30";
                        }
                        else if (i < (model.ColE0To30 + model.ColE31To60))
                        {
                            colE = "31 - 60";
                        }
                        else if (i < (model.ColE0To30 + model.ColE31To60 + model.ColE61To90))
                        {
                            colE = "61 - 90";
                        }
                        else colE = "91+";

                        // append data to row
                        if (layoutType == 1)
                        {
                            // remove the trailing comma to avoid extra columns
                            outputFileBuilder.AppendLine(row.Trim().TrimEnd(',').Split(',')[0] + $",{colA},{colB},{colC},{colD},{colE}");
                        }
                        else if (layoutType == 2)
                        {
                            // remove the trailing comma to avoid extra columns
                            outputFileBuilder.AppendLine(String.Join(",", row.Trim().TrimEnd(',').Split(',').Take(18).ToArray()) + $",{colA},{colB},{colC},{colD},{colE}");
                        }
                    }
                    #endregion

                    CreateNeccessaryFolders();

                    string inputPath = "~/InputFiles/";
                    var extension = Path.GetExtension(model.CsvInputFile.FileName);
                    string ordinaryFileName = Path.GetFileNameWithoutExtension(model.CsvInputFile.FileName);
                    string myFileName = ordinaryFileName + "_" + Guid.NewGuid() + extension;

                    var path = Path.Combine(Server.MapPath(inputPath), myFileName);
                    model.CsvInputFile.SaveAs(path);

                    // save output file
                    string outputPath = "~/OutputFiles/" + fileName + "_processed_" + Guid.NewGuid() + ".csv";
                    System.IO.File.AppendAllText(Server.MapPath(outputPath), outputFileBuilder.ToString());

                    DataFile dataFile = new DataFile
                    {
                        ColA = model.ColA,
                        ColB = model.ColB,
                        ColC = model.ColC,
                        ColD = model.ColD,
                        ColE0To30 = model.ColE0To30,
                        ColE31To60 = model.ColE31To60,
                        ColE61To90 = model.ColE61To90,
                        ColE91Plus = model.ColE91Plus,
                        CampaignName = model.Name,
                        InputFileUri = inputPath + myFileName,
                        OutputFileUri = outputPath,
                        NumberOfRecords = numberOfRecords
                    };
                    ctx.DataFiles.Add(dataFile);
                    ctx.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var theDataFile = ctx.DataFiles.Find(id);
            if (theDataFile == null) return HttpNotFound();

            EditCampaignViewModel model = new EditCampaignViewModel
            {
                ColA = theDataFile.ColA,
                ColB = theDataFile.ColB,
                ColC = theDataFile.ColC,
                ColD = theDataFile.ColD,
                ColE0To30 = theDataFile.ColE0To30,
                ColE31To60 = theDataFile.ColE31To60,
                ColE61To90 = theDataFile.ColE61To90,
                ColE91Plus = theDataFile.ColE91Plus,
                InputFilePath = theDataFile.InputFileUri,
                OutputFilePath = theDataFile.OutputFileUri,
                Name = theDataFile.CampaignName,
                ID = theDataFile.ID,
                Qty = theDataFile.NumberOfRecords
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditCampaignViewModel model)
        {
            StringBuilder outputFileBuilder = new StringBuilder();
            // you can't change input file during update, simply create a new one
            if (ModelState.IsValid)
            {
                var dataFile = ctx.DataFiles.Find(model.ID);
                if (dataFile == null)
                {
                    ModelState.AddModelError("", "Data file records not found");
                    return View(model);
                }

                using (var reader = new StreamReader(Server.MapPath(model.InputFilePath)))
                {
                    #region Read and verify Input Data with formdata
                    // get the actual csv data as string and then store in array
                    string dataString = reader.ReadToEnd().Trim();
                    var dataArray = dataString.Split('\n');

                    // verify input data
                    int numberOfRecords = dataArray.Count() - 1; // the first record is the header
                    // colA <= colB <= colC <= colD <= number of records
                    if (model.ColA > numberOfRecords || model.ColB > numberOfRecords || model.ColC > numberOfRecords || model.ColD > numberOfRecords)
                    {
                        ModelState.AddModelError("", "Values of colA to colD can not be greater than the number of records in file");
                        return View(model);
                    }
                    if (model.ColE0To30 + model.ColE31To60 + model.ColE61To90 + model.ColE91Plus > numberOfRecords)
                    {
                        ModelState.AddModelError("", "Column E records can not be more than the number of records in file");
                        return View(model);
                    }

                    // Take the header => the first row
                    var firstRowHeader = dataArray[0].Trim();

                    // Randomize the data skipping the first row
                    // some rows may contain only commas
                    Random rnd = new Random();
                    var randomizedArray = dataArray.Skip(1).Where(d => !String.IsNullOrWhiteSpace(d.Replace(',', ' '))).OrderBy(x => rnd.Next()).ToArray();
                    #endregion

                    #region check header and layout type
                    // check the first row if it contians col A to E
                    // first layout 5 columns,  second layout 18 columns
                    if (firstRowHeader.Trim().EndsWith(","))
                    {
                        firstRowHeader = firstRowHeader.Trim().TrimEnd(',');
                    }

                    int numberOfColumns = firstRowHeader.Split(',').Count();

                    int layoutType = 0; bool extraColumnsAlreadyAppended = false;
                    // append extra columns to header
                    firstRowHeader += ",Open,Click,Bounced,Optout,Filler1";
                    outputFileBuilder.AppendLine(firstRowHeader);
                    #endregion

                    #region process data in loop
                    // loop through the data and append X where neccessary
                    int countC = 0; int countD = 0;
                    for (int i = 0; i < randomizedArray.Length; i++)
                    {
                        string row = randomizedArray[i];
                        var rowColumns = row.Split(',');
                        string colA = model.ColA > i ? "Y" : "";
                        string colB = model.ColB > i ? "Y" : "";

                        string colC = "";
                        if (colA == "" && colB == "" && countC < model.ColC)
                        {
                            colC = "Y";
                            countC++;
                        }
                        string colD = "";
                        if (colA == "" && colB == "" && colC == "" && countD < model.ColD)
                        {
                            colD = "Y";
                            countD++;
                        }

                        string colE = string.Empty;
                        if (i < model.ColE0To30)
                        {
                            colE = "0 - 30";
                        }
                        else if (i < (model.ColE0To30 + model.ColE31To60))
                        {
                            colE = "31 - 60";
                        }
                        else if (i < (model.ColE0To30 + model.ColE31To60 + model.ColE61To90))
                        {
                            colE = "61 - 90";
                        }
                        else colE = "91+";

                        // append data to row
                        outputFileBuilder.AppendLine(String.Join(",", row.Trim().TrimEnd(',').Split(',').Take(numberOfColumns).ToArray()) + $",{colA},{colB},{colC},{colD},{colE}");
                    }// end of loop
                     #endregion

                    #region update processed file in disk
                    // check if old output file exists

                    string fileName = Path.GetFileNameWithoutExtension(model.InputFilePath);
                    // save new file
                    string outputPath = "~/OutputFiles/" + fileName + "_Match_QTY" + dataFile.NumberOfRecords + ".csv"; // "~/OutputFiles/" + ordinaryFileName + "_processed_" + Guid.NewGuid() + ".csv";
                    RemoveFileFromDisk(outputPath); // delete old file
                    System.IO.File.AppendAllText(Server.MapPath(outputPath), outputFileBuilder.ToString());

                    // delete old file: system does this automatically 
                    // RemoveFileFromDisk(model.OutputFilePath);
                    #endregion

                    #region update datafile in db
                    // now update datafile
                    dataFile.OutputFileUri = outputPath;
                    dataFile.CampaignName = model.Name;
                    dataFile.ColA = model.ColA;
                    dataFile.ColB = model.ColB;
                    dataFile.ColC = model.ColC;
                    dataFile.ColD = model.ColD;
                    dataFile.ColE0To30 = model.ColE0To30;
                    dataFile.ColE31To60 = model.ColE31To60;
                    dataFile.ColE61To90 = model.ColE61To90;
                    dataFile.ColE91Plus = model.ColE91Plus;
                    dataFile.OrderId = model.OrderId;
                    ctx.Entry(dataFile).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                    #endregion

                }

                return RedirectToAction("Index");
            }
            return View(model);
        }

        private string GetOrdinaryFileNameFromInputFileName(string inputFilePath)
        {
            throw new NotImplementedException();
            // return inputFilePath.Split('_')[0];
        }
        private void RemoveFileFromDisk(string fileUri)
        {
            if (System.IO.File.Exists(Server.MapPath(fileUri)))
            {
                System.IO.File.Delete(Server.MapPath(fileUri));
            }
        }

        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var theDataFile = ctx.DataFiles.Find(id);
            if (theDataFile == null) return HttpNotFound();

            return View(theDataFile);
        }

        // download input file
        public ActionResult Dli(string fileName)
        {
            String filePath = Server.MapPath("~/InputFiles/") + Path.GetFileName(fileName);

            return File(filePath, "application/csv", Path.GetFileName(fileName));
        }

        // Download output file
        public ActionResult Dlo(string originalFileName, string downloadFileName)
        {
            String filePath = Server.MapPath("~/OutputFiles/") + Path.GetFileName(originalFileName);
            downloadFileName = downloadFileName.ToLower().EndsWith(".csv") ? downloadFileName : downloadFileName + ".csv";
            return File(filePath, "application/csv", downloadFileName);
        }
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var theDataFile = ctx.DataFiles.Find(id);
            if (theDataFile == null) return HttpNotFound();

            // delete input and processed files
            RemoveFileFromDisk(theDataFile.InputFileUri);
            RemoveFileFromDisk(theDataFile.OutputFileUri);

            ctx.DataFiles.Remove(theDataFile);
            ctx.SaveChanges();
            return RedirectToAction("Index");
        }
        private void CreateNeccessaryFolders()
        {
            CreateFolderIfNotExists("~/InputFiles/");
            CreateFolderIfNotExists("~/OutputFiles/");
        }
        private void CreateFolderIfNotExists(string folderPath)
        {
            if (!System.IO.Directory.Exists(Server.MapPath(folderPath)))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath(folderPath));
            }
        }
    }
}