﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JSilverCsv.Models
{
    public class DataFile
    {
        public int ID { get; set; }
        public int OrderId { get; set; }
        public string CampaignName { get; set; }
        public int ColA { get; set; }  
        public int ColB { get; set; } 
        public int ColC { get; set; }  
        public int ColD { get; set; }  
        public int ColE0To30 { get; set; } 
        public int ColE31To60 { get; set; } 
        public int ColE61To90 { get; set; }  
        public int ColE91Plus { get; set; }
        public string InputFileUri { get; set; }
        public string OutputFileUri { get; set; }
        public int NumberOfRecords { get; set; }
    }
}